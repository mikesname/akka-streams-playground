package com.example

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, Materializer}
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.WordSpecLike
import org.scalatest.Matchers
import org.scalatest.BeforeAndAfterAll

import scala.concurrent.Await
import scala.concurrent.duration._

class OaiPmhDownloaderSpec(_system: ActorSystem) extends TestKit(_system) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {

  def this() = this(ActorSystem("MySpec"))
 
  override def afterAll {
    Await.ready(system.terminate(), 10.seconds)
  }
 
  "Downloader must" must {
    "download documents" in {
      implicit val mat: Materializer = ActorMaterializer()

      val downloader: OaiPmhDownloader = OaiPmhDownloader(
        "https://eprints.soas.ac.uk/cgi/oai2")

      println(Await.ready(downloader
        .downloadDocuments
        .takeWithin(10.seconds)
        .runForeach(n =>
          println((n \ "metadata" \ "dc" \ "title").
            headOption.map(_.text))), 300.seconds))

    }
  }
}
