package com.example

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.headers.{BasicHttpCredentials, Location}
import akka.stream.scaladsl.Sink
import akka.stream.{ActorMaterializer, Materializer}
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.concurrent.Await
import scala.concurrent.duration._

class GithubIssueDownloaderSpec(_system: ActorSystem) extends TestKit(_system) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {

  def this() = this(ActorSystem("MySpec"))

  implicit val mat: Materializer = ActorMaterializer()
  implicit val ec = mat.executionContext

  "Downloader must" must {
    "download documents" in {
      val downloader: GithubIssueDownloader = GithubIssueDownloader(
        "https://api.github.com/repos/akka/akka/issues",
        "mikesname", "a1656e54229d8fc23083c792cf95063cd3485e46")

      println(Await.ready(downloader
        .downloadDocuments
        .map { case (uri, r) =>
          println(s"uri: $uri, status: ${r.status}")
          r.status
        }
        .runWith(Sink.lastOption), 300.seconds))

    }
  }
}
