package com.example

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Location
import akka.stream.Materializer
import akka.stream.scaladsl.{Flow, Source}
import akka.util.ByteString

import scala.concurrent.Future
import scala.xml.{Elem, Node, XML}

case class OaiPmhDownloader(baseUri: String)(implicit val system: ActorSystem, mat: Materializer) {

  implicit val executionContext = mat.executionContext

  val standardParams = Seq("verb" -> "ListRecords", "metadataPrefix" -> "oai_dc")

  def responseData(r: HttpResponse): Future[Elem] = {
    r.entity.withoutSizeLimit().dataBytes.runFold(ByteString.empty)(_ ++ _)
      .map(s => XML.loadString(s.utf8String))
  }

  def uri: Uri = Uri(baseUri)
    .withQuery(Uri.Query(standardParams: _*))

  def initial: HttpRequest = HttpRequest(HttpMethods.GET, uri)

  def nextUri(resumptionToken: String): Uri = uri
    .withQuery(Uri.Query(
      ("resumptionToken" -> resumptionToken) +: standardParams: _*))

  def responseDataToRequest(xml: Elem): Option[(HttpRequest, Elem)] =
    (xml \ "ListRecords" \ "resumptionToken").headOption.map { n =>
      HttpRequest(HttpMethods.GET, nextUri(n.text)) -> xml
    }

  def responseToNodeFlow: Flow[Elem, Node, akka.NotUsed] = Flow[Elem]
    .map(xml => (xml \ "ListRecords" \ "record").toList)
    .flatMapConcat(n => Source(n))

  def xmlExtractor(r: HttpResponse): Future[Option[(HttpRequest, Elem)]] =
    responseData(r).map(responseDataToRequest)

  def responseExtractor(r: HttpResponse): Future[Option[(HttpRequest, HttpResponse)]] =
    Future.successful(r.header[Location].map { loc =>
      HttpRequest(HttpMethods.GET, loc.uri) -> r
    })

  val GithubNextLinkExtractor = """<[^>]+>; rel="next";""".r


  def chainRequests[T](init: HttpRequest)(extractor: HttpResponse => Future[Option[(HttpRequest, T)]]): Source[T, akka.NotUsed] =
    Source
      .unfoldAsync[HttpRequest, T](init) { req =>
    Http().singleRequest(req).flatMap { response =>
      if (response.status.isFailure())
        Future.successful(Option.empty[(HttpRequest, T)])
      else extractor(response)
    }
  }

  def downloadDocuments: Source[Node, akka.NotUsed] = chainRequests(initial)(xmlExtractor)
    .via(responseToNodeFlow)
}