package com.example

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.{Authorization, BasicHttpCredentials, Link}
import akka.stream.Materializer
import akka.stream.scaladsl.Source

import scala.concurrent.duration._
import scala.concurrent.Future

case class GithubIssueDownloader(baseUri: String, username: String, token: String)(
    implicit val system: ActorSystem, mat: Materializer) {

  implicit val executionContext = mat.executionContext

  def credentials = BasicHttpCredentials(username, token)

  def initialRequest: HttpRequest = HttpRequest(HttpMethods.GET, baseUri)
    .withHeaders(Authorization(credentials))

  def convertToStrict(r: HttpResponse): Future[HttpResponse] =
    r.entity.toStrict(10.minutes).map(e => r.withEntity(e))

  def nextUri(r: HttpResponse): Seq[Uri] = for {
    linkHeader <- r.header[Link].toSeq
    value <- linkHeader.values
    params <- value.params if params.key == "rel" && params.value() == "next"
  } yield value.uri

  def responseExtractor(ruri: Uri, r: HttpResponse): Future[Option[(Option[HttpRequest], (Uri, HttpResponse))]] =
    convertToStrict(r).map { strictResponse =>
      nextUri(strictResponse).headOption match {
        case Some(uri) => Some(Some(HttpRequest(HttpMethods.GET, uri)
            .withHeaders(Authorization(credentials))) -> (ruri -> strictResponse))
        case None => Some(Option.empty[HttpRequest], ruri -> strictResponse)
      }
    }

  def checkForNext(reqOption: Option[HttpRequest]): Future[Option[(Option[HttpRequest], (Uri, HttpResponse))]] =
    reqOption match {
      case Some(req) => Http().singleRequest(req).flatMap { response =>
        if (response.status.isFailure())
          Future.successful(Some(Option.empty, req.uri -> response))
        else responseExtractor(req.uri, response)
      }
      case None => Future.successful(Option.empty[(Option[HttpRequest], (Uri, HttpResponse))])
    }


  def downloadDocuments: Source[(Uri, HttpResponse), akka.NotUsed] =
    Source.unfoldAsync[Option[HttpRequest],
      (Uri, HttpResponse)](Some(initialRequest))(checkForNext)
}