name := """oai-pmh-downloader"""

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "org.scala-lang.modules" %% "scala-xml" % "1.0.3",
  "com.typesafe.akka" %% "akka-stream" % "2.4.10",
  "com.typesafe.akka" %% "akka-http-experimental" % "2.4.10",
  "com.typesafe.akka" %% "akka-http-xml-experimental" % "2.4.10",
  "com.typesafe.akka" %% "akka-stream-testkit" % "2.4.10" % "test",
  "com.typesafe.akka" %% "akka-http-testkit" % "2.4.10" % "test",
  "org.scalatest" %% "scalatest" % "2.2.4" % "test"
)
